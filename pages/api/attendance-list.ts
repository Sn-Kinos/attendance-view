import { NextApiRequest, NextApiResponse } from 'next';
import { Pool } from 'pg';

const pool = new Pool({
  host: process.env.DB_HOSTNAME,
  port: Number(process.env.DB_PORT),
  database: process.env.POSTGRES_DB,
  user: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  max: 5,
  idleTimeoutMillis: 30000,
  connectionTimeoutMillis: 2000,
});

async function asyncQuery(query: string, optionArray: string[]) {
  let conn;

  try {
    conn = await pool.connect();
    let rows;
    if (optionArray) {
      rows = await conn.query(query, optionArray);
    } else {
      rows = await conn.query(query);
    }
    return rows;
  } catch (err) {
    throw err;
  } finally {
    if (conn) {
      conn.release();
    }
  }
}

export default async (req: NextApiRequest, res: NextApiResponse) => {
  console.log(req.query);
  let dateQuery: string = '';
  const queryParams: string[] = [req.query.name as string];

  if (req.query.start !== 'undefined' && req.query.end !== 'undefined') {
    dateQuery = `AND log.created_at BETWEEN $2 AND $3;`;
    queryParams.push(req.query.start as string, req.query.end as string);
  }

  try {
    const result = await asyncQuery(
      `SELECT username, log.created_at AT TIME ZONE 'Asia/Seoul'
      FROM attendance.log, attendance.user
      WHERE attendance.user.rfid = attendance.log.rfid
      AND username = $1
      ${dateQuery}`,
      queryParams
    );

    console.log(result.rows);

    res.status(200).json(result.rows);
  } catch (e) {
    console.error(e);
    
    res.status(500).send(e);
  }
};
