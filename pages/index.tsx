import styled from 'styled-components';
import fetch from 'node-fetch';
import { ChangeEventHandler, FormEventHandler, useState } from 'react';
import { CSVLink } from 'react-csv';

const Title = styled.h1`
  text-align: center;
  font-size: 50px;
`;

interface IAttendanceData {
  username: string;
  timezone: string;
}

export default function Home() {
  const [name, setName] = useState<string>('');
  const [start, setStart] = useState<Date>();
  const [end, setEnd] = useState<Date>();
  const [attendanceList, setAttendanceList] = useState<IAttendanceData[]>([]);

  const handleSubmit: FormEventHandler<HTMLFormElement> = async (e) => {
    e.preventDefault();
    const result = await fetch(`/api/attendance-list?name=${name}&start=${start?.toDateString()}&end=${end?.toDateString()}`);
    setAttendanceList(
      (await result.json()).map((v: IAttendanceData) => ({
        ...v,
        timezone: new Date(v.timezone).toLocaleString('ko-KR'),
      }))
    );
  };

  const changeName: ChangeEventHandler<HTMLInputElement> = (e) => {
    setName(e.target.value);
  };

  const changeStart: ChangeEventHandler<HTMLInputElement> = (e) => {
    setStart(new Date(e.target.value));
  };

  const changeEnd: ChangeEventHandler<HTMLInputElement> = (e) => {
    setEnd(new Date(e.target.value));
  };

  return (
    <main>
      <header>
        <Title>DDPillow Attendance</Title>
      </header>
      <section>
        <h2>출석 조회</h2>
        <form onSubmit={handleSubmit}>
          <div>
            <label htmlFor="username">이름</label>
            <input type="text" id="username" name="username" placeholder="" onChange={changeName} />
          </div>
          <div>
            <label htmlFor="start">시작일</label>
            <input type="date" name="start" id="start" onChange={changeStart} />
          </div>
          <div>
            <label htmlFor="end">종료일</label>
            <input type="date" name="end" id="end" onChange={changeEnd} />
          </div>
          <input type="submit" />
          {attendanceList.length ? (
            <CSVLink filename={`${name}_${start ?? ''}_${end ?? ''}.csv`} data={attendanceList}>
              CSV 다운
            </CSVLink>
          ) : null}
        </form>
        <dl>
          {attendanceList.map((v) => (
            <div key={v.timezone}>
              <dt>{v.username}</dt>
              <dd>{v.timezone}</dd>
            </div>
          ))}
        </dl>
      </section>
    </main>
  );
}
